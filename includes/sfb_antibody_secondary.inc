<?php
/**
 * Created by PhpStorm.
 * User: freckmann8
 * Date: 23.09.2016
 * Time: 12:50
 */

/**
 * Primary antibodies overview.
 *
 * This site prints all primary antibodies.
 *
 * @return string
 */
function sfb_antibody_secondary() {

  //
  // prepare and print the secondary antibodies table
  //

  // table header
  // columns with 'field' definition can be sorted
  $header = array(
    array('data' => t('AG'), 'field' => 'a.working_group_id'),
    array('data' => t('PID'), 'field' => 'a.id', 'sort' => 'desc'),
    array('data' => t('Antibody Registry')),
    array('data' => t('Name'), 'field' => 'a.name'),
    array('data' => t('Excitation')),
    array('data' => t('Quality')),
    array('data' => t('Company')),
    array('data' => t('Catalog no.')),
  );

  // get all secondary antibodies from database
  // use table sort and pager function
  $antibodies = AntibodiesRepository::findByTypeUseTableSortAndUsePagerDefault(AntibodyType::SECONDARY, $header, variable_get(SFB_ANTIBODY_CONFIG_OVERVIEW_NO_OF_ANTIBODIES, 25));

  // create table rows
  $rows = array();

  // each table row is a table row
  foreach($antibodies as $antibody) {

    // prepare an array with applications and their quality
    // variable $apps_arr will be later on used for quality collapse theme
    // @see theme_sfb_antibody_quality_collapsible()
    $apps_arr = array();
    $apps = AntibodyAntibodyApplicationsRepository::findByAntibodyIdGroupByApplication($antibody->getId());
    foreach($apps as $app) {
      $apps_arr[] = array(
        'quality' => $antibody->getApplicationQuality($app->getApplicationAbbr()),
        'application_abbr' => $app->getApplicationAbbr(),
      );
    }

    // fill table rows with antibody data
    $rows[] = array(
      // field: reseaech group
      $antibody->getElementWorkingGroupIcon(),
      // field: antibody pid and link to antibody view
      l($antibody->getElementPID(), sfb_antibody_url(SFB_ANTIBODY_URL_SECONDARY_VIEW, $antibody->getElementPID())),
      // field: antibody registry
      $antibody->getElementRegistryId(true),
      // field: name
      $antibody->getElementName(),
      //field: clonality
      $antibody->getElementExcitationMax(),
      // collapsible field with applications rating
      array('data' => theme('sfb_antibody_quality_collapsible', array('antibody_id'=> $antibody->getId(), 'applications_quality' => $apps_arr)), 'style' => 'min-width: '.variable_get(SFB_ANTIBODY_CONFIG_STYLE_QUALITY_WIDTH, 100).'px;' ),

      // field: company
      $antibody->getElementCompany(),
      // field: catalog no.
      $antibody->getElementCatalogNo(),
    );
  }

  $output = '';

  // print search box
  $search_form = drupal_get_form('sfb_anitbody_form_search', '', array('type' => 'secondary'));
  $output .= generateImportExportButtons().drupal_render($search_form);

  $output .= theme('table', array('header' => $header, 'rows' => $rows)).theme('pager', array('tags' => array()));

  if(user_access(SFB_ANTIBODY_PERMISSION_REGISTRAR))
    $output .= '<a class="btn btn-primary" href="'. sfb_antibody_url(SFB_ANTIBODY_URL_SECONDARY_NEW) .'">Register new secondary antibody</a>';

  return $output;
}
