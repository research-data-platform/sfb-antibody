<?php

function sfb_antibody_access($pid) {

  if(empty($pid)) {
    drupal_not_found();
    exit();
  }

  $antibody_id = Antibody::extractIDFromPID($pid);

  $antibody = AntibodiesRepository::findById($antibody_id);

  if($antibody->isEmpty()) {
    drupal_not_found();
    exit();
  }

  switch($antibody->getType()) {
    case AntibodyType::PRIMARY:
      drupal_goto(sfb_antibody_url(SFB_ANTIBODY_URL_PRIMARY_VIEW, $antibody->getElementPID()));
      break;
    case AntibodyType::SECONDARY:
      drupal_goto(sfb_antibody_url(SFB_ANTIBODY_URL_SECONDARY_VIEW, $antibody->getElementPID()));
      break;
    default:
      watchdog('sfb_antibody', 'Unknown antibody type (sfb_antibody_acess.inc)');
      drupal_not_found();
      exit();
      break;
  }
}