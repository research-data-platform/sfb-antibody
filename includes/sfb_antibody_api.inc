<?php

/**
 *  Main API handler.
 */
function sfb_antibody_api() {

  // Since version 2.0 of antibody module api version is a part of the url
  // Autocomplete paths are not specific for any api version

  $api = arg(2);        // version of the api called or 'autocomplete'
  $call = arg(3);       // api call
  $parameter1 = arg(4); // optional: parameter1
  $parameter2 = arg(5); // optional: parameter1


  if ($api == '2') {

    switch ($call) {
      case 'antibody':
        return sfb_antibody_api_antibody($parameter1);
      case 'search-antibody':
        // ToDo: Should this search primary and secondary ABs? -Markus, 2018-04-19
        return sfb_antibody_api_search_antibody2($parameter1);
      case 'search-primary-antibody':
        return sfb_antibody_api_search_primary_antibody($parameter1);
      case 'search-secondary-antibody':
        return sfb_antibody_api_search_secondary_antibody($parameter1);
      case 'species':
        return sfb_antibody_api_species($parameter1);
      default:
        return 'Unknown api call';
    }


  }
  else {
    if ($api == 'autocomplete') {
      switch ($call) {
        case 'application':
          return sfb_antibody_api_autocomplete_application($parameter1);
        case 'tag':
          return sfb_antibody_api_autocomplete_tag($parameter1);
        case 'species':
          return sfb_antibody_api_autocomplete_species($parameter1);
        case 'company':
          return sfb_antibody_api_autocomplete_company($parameter1);
      }

    }
    else {
      drupal_not_found();
      exit();
    }
  }
}

/**
 * API handler for antibodies species
 *
 * @param string $string
 */
function sfb_antibody_api_species($string = '') {
  $matches = [];

  if ($string) {
    $db_or = db_or();
    $db_or->condition('name', db_like($string) . '%', 'LIKE');

    $species = AntibodySpeciesRepository::findBy($db_or);

    foreach ($species as $spc) {
      $matches[] = [
        'version' => SFB_ANTIBODY_API_VERSION,
        'id' => check_plain($spc->getId()),
        'name' => check_plain($spc->getName()),
      ];
    }

  }

  drupal_json_output($matches);
}

/**
 * API handler for antibodies tags
 *
 * @param string $string
 */
function sfb_antibody_api_tag($string = '') {
  $matches = [];

  if ($string) {
    $db_or = db_or();
    $db_or->condition('name', db_like($string) . '%', 'LIKE');

    $tags = AntibodyTagRepository::findBy($db_or);

    foreach ($tags as $tag) {
      $matches[] = [
        'version' => SFB_ANTIBODY_API_VERSION,
        'id' => check_plain($tag->getId()),
        'name' => check_plain($tag->getName()),
      ];
    }

  }

  drupal_json_output($matches);
}

/**
 * API handler for antibodies
 *
 * @param string $string
 */
function sfb_antibody_api_antibody($string = '') {

  $matches = [];

  if ($string) {
    $db_or = db_or();
    $db_or->condition('id', Antibody::extractIDFromPID($string), '=');

    $antibodies = AntibodiesRepository::findBy($db_or);

    foreach ($antibodies as $antibody) {
      $matches[] = [
        'version' => SFB_ANTIBODY_API_VERSION,
        '@context' => 'http://schema.org/',
        '@type' => 'Thing',
        'additionalType' => 'Antibody',
        'id' => check_plain($antibody->getId()),
        'pid' => check_plain($antibody->getElementPID()),
        'epic_pid' => $antibody->getElementEPICPID(),
        'research_group' => $antibody->getElementWorkingGroup(),
        'lab_id' => check_plain($antibody->getLabId()),
        'type' => check_plain($antibody->getType()),
        'antigen_symbol' => check_plain($antibody->getAntigenSymbol()),
        'antibody_registry_id' => $antibody->getRegistryId(),
        'name' => check_plain($antibody->getName()),
        'alternative_name' => check_plain($antibody->getAlternativeName()),
        'tag_flurophore' => check_plain($antibody->getElementTag()),
        'raised_id' => check_plain($antibody->getElementRaisedIn()),
        'reacts_with' => check_plain($antibody->getElementReactsWith()),
        'clone' => check_plain($antibody->getElementClone()),
        'isotype' => check_plain($antibody->getElementIsotype()),
        'clonality' => check_plain($antibody->getElementClonality()),
        'demasking' => check_plain($antibody->getElementDemasking()),
        'antigen' => check_plain($antibody->getElementAntigen()),
        //TODO: 'crafted by' not supported
        'company' => check_plain($antibody->getElementCompany()),
        'catalog_no' => check_plain($antibody->getElementCatalogNo()),
        'lot_no' => check_plain($antibody->getElementLotNo()),
        'description' => check_plain($antibody->getElementDescription()),
        //TODO: 'localization', 'storage instruction'
        'last_modified' => check_plain($antibody->getElementLastModified()),
      ];
    }
  }

  drupal_json_output($matches);
}

/**
 * API handler for antibodies searching. Searches only primary antibodies
 *
 * @param string $string searched antibody
 */
function sfb_antibody_api_search_primary_antibody($string = '') {

  $matches = [];

  if ($string) {
    $search_patterns = db_or();
    $search_patterns->condition('id', Antibody::extractIDFromPID($string), '=');
    $search_patterns->condition('name', '%' . db_like($string) . '%', 'LIKE');
    $search_patterns->condition('alt_name', db_like($string) . '%', 'LIKE');
    $search_patterns->condition('antigen_symbol', db_like($string) . '%', 'LIKE');

    // limit the search to Primary Antibodies
    $query = db_and();
    $query->condition('type', 'primary', '=');
    $query->condition($search_patterns);

    $antibodies = AntibodiesRepository::findBy($query);

    $matches = sfb_antibody_api_search_antibody_create_matches_array($antibodies);

  }

  drupal_json_output($matches);
}

/**
 * API handler for antibodies searching. Searches only secondary antibodies
 *
 * @param string $string searched antibody
 */
function sfb_antibody_api_search_secondary_antibody($string = '') {

  $matches = [];

  // TODO 16/04/18: add like for raised_in_species, excitation_max
  if ($string) {
    $search_patterns = db_or();
    $search_patterns->condition('id', Antibody::extractIDFromPID($string), '=');
    $search_patterns->condition('name', '%' . db_like($string) . '%', 'LIKE');
    $search_patterns->condition('alt_name', db_like($string) . '%', 'LIKE');
    $search_patterns->condition('raised_in_species', db_like($string) . '%', 'LIKE');
    $search_patterns->condition('excitation_max', '%' . db_like($string) . '%', 'LIKE');

    // Limit the search to Secondary Antibodies
    $query = db_and();
    $query->condition('type', 'secondary', '=');
    $query->condition($search_patterns);

    $antibodies = AntibodiesRepository::findBy($query);

    $matches = sfb_antibody_api_search_antibody_create_matches_array($antibodies);

  }

  drupal_json_output($matches);
}


/**
 * API handler for antibodies searching. Searches primary and secondary
 * antibodies
 *
 * @param string $string searched antibody
 */
function sfb_antibody_api_search_antibody2($string = '') {

  $matches = [];

  if ($string) {
    $search_patterns = db_or();
    $search_patterns->condition('id', Antibody::extractIDFromPID($string), '=');
    $search_patterns->condition('name', '%' . db_like($string) . '%', 'LIKE');
    $search_patterns->condition('alt_name', db_like($string) . '%', 'LIKE');
    $search_patterns->condition('antigen_symbol', db_like($string) . '%', 'LIKE');

    $antibodies = AntibodiesRepository::findBy($search_patterns);

    $matches = sfb_antibody_api_search_antibody_create_matches_array($antibodies);

  }

  drupal_json_output($matches);
}

/**
 * Creates a array with the values from a antibody (object) array.
 * Fields:
 * 'version'
 * 'id'
 * 'type'
 * 'pid'
 * 'name'
 * 'alternative_name'
 * 'antigen_symbol'
 * 'research_group'
 * 'antibody_registry_id'
 * 'catalog_no'
 *
 * @param $antibodies
 *
 * @return array
 */
function sfb_antibody_api_search_antibody_create_matches_array($antibodies) {
  $matches = [];

  foreach ($antibodies as $antibody) {
    $matches[] = [
      'version' => SFB_ANTIBODY_API_VERSION,
      'id' => check_plain($antibody->getId()),
      'type' => check_plain($antibody->getType()),
      'pid' => check_plain($antibody->getElementPID()),
      'name' => check_plain($antibody->getName()),
      'alternative_name' => check_plain($antibody->getAlternativeName()),
      'antigen_symbol' => check_plain($antibody->getAntigenSymbol()),
      'research_group' => check_plain($antibody->getElementWorkingGroup()),
      'antibody_registry_id' => '', //TODO: to be implemented
      'catalog_no' => check_plain($antibody->getCatalogNo()),
      'raised_in_species' => check_plain($antibody->getElementRaisedIn()),
      'excitation_max' => check_plain($antibody->getElementExcitationMax()),
    ];
  }

  return $matches;
}

function sfb_antibody_api_autocomplete_species($string = '') {
  $matches = [];

  if ($string) {
    $db_or = db_or();
    $db_or->condition('name', db_like($string) . '%', 'LIKE');

    $species = AntibodySpeciesRepository::findBy($db_or);

    foreach ($species as $spc) {
      $matches[check_plain($spc->getName())] = check_plain($spc->getName());
    }

  }

  drupal_json_output($matches);
}

/**
 * Autocomplete function for tags and fluorophores
 *
 * @param string $string
 * Searched tag name or fluorophore
 *
 * @return json string
 */
function sfb_antibody_api_autocomplete_tag($string = '') {
  $matches = [];

  if ($string) {

    $tags = AntibodiesRepository::findTags($string);

    foreach ($tags as $tag) {
      $matches[check_plain($tag->tag)] = check_plain($tag->tag);
    }

  }

  drupal_json_output($matches);

}

/**
 * Autocomplete function for applications.
 *
 * @param string $string
 *  Searched application name or abbreviation
 *
 * @return json string
 *
 */
function sfb_antibody_api_autocomplete_application($string = '') {
  $matches = [];

  if ($string) {
    $applications = AntibodyApplicationsRepository::findLikeByAbbreviationOrByName($string);

    foreach ($applications as $application) {
      $matches[$application->getAbbreviation()] = check_plain($application->getAbbreviation());
    }

  }

  drupal_json_output($matches);
}

/**
 * Autocomplete function for companies.
 *
 * @param string $string
 *   Searched company name
 *
 * @return json string
 */
function sfb_antibody_api_autocomplete_company($string = '') {
  $matches = [];

  if ($string) {
    $companies = AntibodiesRepository::findCompanies($string);

    foreach ($companies as $company) {
      $matches[check_plain($company->company)] = check_plain($company->company);
    }
  }

  drupal_json_output($matches);
}