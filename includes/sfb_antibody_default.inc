<?php

function sfb_antibody_default() {

  // Set the title for this page
  drupal_set_title('Antibody Catalogue');

  $output = '';

  // print a message about number of antibodies registered in antibody catalogue
  $antibodies_number = AntibodiesRepository::getAntibodiesNumber();
  $antibodies_primary_number = AntibodiesRepository::getAntibodiesNumber(AntibodyType::PRIMARY);
  $antibodies_secondary_number = AntibodiesRepository::getAntibodiesNumber(AntibodyType::SECONDARY);

  $stats = 'The catalogue currently contains '.$antibodies_number.' antibodies (primary: '.$antibodies_primary_number.', secondary: '.$antibodies_secondary_number.').<div>&nbsp;</div>';

  $output .= '
    <div class="row">
       <div class="col-sm-12 text-left">
       ' . $stats . ' 
       </div>
    </div>'
  ;
  // print search box
  $search_form = drupal_get_form('sfb_anitbody_form_search');
  $output .= generateImportExportButtons(). drupal_render($search_form);

  //
  //
  // prepare and print the primary antibodies table with 5 newest entries
  //
  //

  // table header
  // columns with 'field' definition can be sorted
  $header = array(
    array('data' => t('AG')),
    array('data' => t('PID')),
    array('data' => t('Antigen Symbol')),
    array('data' => t('Antibody Registry')),
    array('data' => t('Name')),
    array('data' => t('Clonality')),
    array('data' => t('Antigen')),
    array('data' => t('Quality')),
    array('data' => t('Company')),
    array('data' => t('Catalog no.')),
  );

  // get last 5 newest primary antibodies
  $primary_a = AntibodiesRepository::findByTypeOrderByIDAndLimit(AntibodyType::PRIMARY, 5);

  // create table rows
  $rows = array();

  // each table row is a table row
  foreach($primary_a as $antibody) {

    // prepare an array with applications and their quality
    // variable $apps_arr will be later on used for quality collapse theme
    // @see theme_sfb_antibody_quality_collapsible()
    $apps_arr = array();
    $apps = AntibodyAntibodyApplicationsRepository::findByAntibodyIdGroupByApplication($antibody->getId());
    foreach($apps as $app) {
      $apps_arr[] = array(
        'quality' => $antibody->getApplicationQuality($app->getApplicationAbbr()),
        'application_abbr' => $app->getApplicationAbbr(),
      );
    }

    // fill table rows with antibody data
    $rows[] = array(
      // field: reseaech group
      $antibody->getElementWorkingGroupIcon(),
      // field: antibody pid and link to antibody view
      l($antibody->getElementPID(), sfb_antibody_url(SFB_ANTIBODY_URL_PRIMARY_VIEW, $antibody->getElementPID())),
      // field: antigen symbol
      $antibody->getElementAntigenSymbol(),
      // field: antibody registry
      $antibody->getElementRegistryId(true),
      // field: name
      $antibody->getElementName(),
      //field: clonality
      $antibody->getElementClonality(),
      // field: antigen
      $antibody->getElementAntigen(),
      // collapsible field with applications rating
      array('data' => theme('sfb_antibody_quality_collapsible', array('antibody_id'=> $antibody->getId(), 'applications_quality' => $apps_arr)), 'style' => 'min-width: '.variable_get(SFB_ANTIBODY_CONFIG_STYLE_QUALITY_WIDTH, 100).'px;' ),
      // field: company
      $antibody->getElementCompany(),
      // field: catalog no.
      $antibody->getElementCatalogNo(),
    );

  }

  $output .= '<h2>New primary antibodies</h2>';
  $output .= theme('table', array('header' => $header, 'rows' => $rows));
  $output .= '<a class="btn btn-default" href="'. sfb_antibody_url(SFB_ANTIBODY_URL_PRIMARY) .'">See all primary antibodies</a> ';
  if(user_access(SFB_ANTIBODY_PERMISSION_REGISTRAR))
    $output .= '<a class="btn btn-primary" href="'. sfb_antibody_url(SFB_ANTIBODY_URL_PRIMARY_NEW) .'">Register new primary antibody</a>';

  //
  //
  // prepare and print the secondary antibodies table with 5 newest entries
  //
  //

  $header = array(
    array('data' => t('AG')),
    array('data' => t('PID')),
    array('data' => t('Antibody Registry')),
    array('data' => t('Name')),
    array('data' => t('Excitation')),
    array('data' => t('Quality')),
    array('data' => t('Company')),
    array('data' => t('Catalog no.')),
  );

  // get last 5 newest primary antibodies
  $secondary_a = AntibodiesRepository::findByTypeOrderByIDAndLimit(AntibodyType::SECONDARY, 5);

  // create table rows
  $rows = array();

  // each table row is a table row
  foreach($secondary_a as $antibody) {

    // prepare an array with applications and their quality
    // variable $apps_arr will be later on used for quality collapse theme
    // @see theme_sfb_antibody_quality_collapsible()
    $apps_arr = array();
    $apps = AntibodyAntibodyApplicationsRepository::findByAntibodyIdGroupByApplication($antibody->getId());
    foreach($apps as $app) {
      $apps_arr[] = array(
        'quality' => $antibody->getApplicationQuality($app->getApplicationAbbr()),
        'application_abbr' => $app->getApplicationAbbr(),
      );
    }

    // fill table rows with antibody data
    $rows[] = array(
      // field: reseaech group
      $antibody->getElementWorkingGroupIcon(),
      // field: antibody pid and link to antibody view
      l($antibody->getElementPID(), sfb_antibody_url(SFB_ANTIBODY_URL_SECONDARY_VIEW, $antibody->getElementPID())),
      // field: antibody registry
      $antibody->getElementRegistryId(true),
      // field: name
      $antibody->getElementName(),
      // field: excitation
      $antibody->getElementExcitationMax(),
      // collapsible field with applications rating
      array('data' => theme('sfb_antibody_quality_collapsible', array('antibody_id'=> $antibody->getId(), 'applications_quality' => $apps_arr)), 'style' => 'min-width: '.variable_get(SFB_ANTIBODY_CONFIG_STYLE_QUALITY_WIDTH, 100).'px;' ),
      // field: company
      $antibody->getElementCompany(),
      // field: catalog no.
      $antibody->getElementCatalogNo(),
    );
  }

  $output .= '<h2>New secondary antibodies</h2>';
  $output .= theme('table', array('header' => $header, 'rows' => $rows));
  $output .= '<a class="btn btn-default" href="'. sfb_antibody_url(SFB_ANTIBODY_URL_SECONDARY) .'">See all secondary antibodies</a> ';

  if(user_access(SFB_ANTIBODY_PERMISSION_REGISTRAR))
    $output .= '<a class="btn btn-primary" href="'. sfb_antibody_url(SFB_ANTIBODY_URL_SECONDARY_NEW) .'">Register new secondary antibody</a>';

  return $output;
}
