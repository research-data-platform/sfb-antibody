<?php

/**
 * Class AntibodyImage
 */
class AntibodyImage {

  const UPLOAD_PATH = 'public://antibody-images';

  /**
   * @var int
   *   The database ID of the antibody image.
   */
  private $id;

  /**
   * @var int
   *   The file ID of the antibody image set by the drupal filesystem.
   */
  private $fid;

  /**
   * @var int
   *   The database ID of the respective antibody.
   */
  private $ab_app_id;

  /**
   * @var string
   *   The short description of the image.
   */
  private $description;

  /**
   * @var string
   *   The comment to the image.
   */
  private $comment;

  /**
   * @var int
   *   The ID of the user who uploaded the image.
   */
  private $uploader;

  /**
   * @var string
   *   The date when the image was uploaded.
   */
  private $upload_date;

  /**
   * @var string
   *   Either 'ab' or 'app'
   */
  private $image_type;

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return int
   */
  public function getFid() {
    return $this->fid;
  }

  /**
   * @param int $fid
   */
  public function setFid($fid) {
    $this->fid = $fid;
  }

  /**
   * @return int
   */
  public function getAbappId() {
    return $this->ab_app_id;
  }

  /**
   * @param int $ab_app_id
   */
  public function setAbappId($ab_app_id) {
    $this->ab_app_id = $ab_app_id;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param string $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * @return string
   */
  public function getComment() {
    return $this->comment;
  }

  /**
   * @param string $comment
   */
  public function setComment($comment) {
    $this->comment = $comment;
  }

  /**
   * @return int
   */
  public function getUploader() {
    return $this->uploader;
  }

  /**
   * @param int $uploader
   */
  public function setUploader($uploader) {
    $this->uploader = $uploader;
  }

  /**
   * @return string
   */
  public function getUploadDate() {
    return $this->upload_date;
  }

  /**
   * @param string $upload_date
   */
  public function setUploadDate($upload_date) {
    $this->upload_date = $upload_date;
  }

  /**
   * @return string
   */
  public function getImageType()
  {
    return $this->image_type;
  }

  /**
   * @param string $image_type
   */
  public function setImageType($image_type)
  {
    $this->image_type = $image_type;
  }

  /**
   * Return an associative array with form control data for 'Image Fieldset' field.
   *
   * @param $i
   *
   * @return array
   */
  public function getFormFieldImageFieldset($i) {
    $count = $i +1;
    return [
      '#type' => 'fieldset',
      '#title' => t('Image') . ' ('.$count.')',
      '#collapsible' => TRUE,
    ];
  }

  /**
   * Return an associative array with form control data for 'Upload' field.
   *
   * @return array
   */
  public function getFormFieldImageUpload($i) {
    return [
      '#type' => 'managed_file',
      '#name' => $i,
      '#upload_location' => self::UPLOAD_PATH,
      '#title' => t('png, jpg, jpeg or gif  |  max. 5 MB'),
      '#default_value' => $this->getFid(),
      '#upload_validators' => array(
        'file_validate_extensions' => ['png jpg jpeg gif'],
        'file_validate_size' => array(5*1024*1024),
      ),
      '#process' => array('sfb_antibody_my_file_element_process'),
    ];
  }

  /**
   * Return an associative array with form control data for 'Description' field.
   *
   * @return array
   */
  public function getFormFieldImageDescription() {
    return [
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#maxlength' => 128,
      '#default_value' => $this->getDescription(),
      '#prefix' => '<div>&nbsp;</div>',
    ];
  }

  /**
   * Return an associative array with form control data for 'Comment' field.
   *
   * @return array
   */
  public function getFormFieldImageComment() {
    return [
      '#type' => 'textarea',
      '#title' => t('Comment'),
      '#maxlength' => 128,
      '#default_value' => $this->getComment(),
      '#rows' => 7,
      '#prefix' => '        
    <div class="inline">
      <div class="col-xs-7">',
      '#suffix' => '</div>  
    </div>',
    ];
  }

  /**
   * Return an associative array with form control data for 'Image Preview' field.
   *
   * @return array
   */
  public function getFormFieldImagePreview () {
    if ($this->getFid()) {
      $file = file_load($this->getFid());
      $info = image_get_info($file->uri);
      $width = $info['width'];
      $height = $info['height'];
      if ($width > 420) {
        $ratio = $width/$height;
        $width = 420;
        $height = $width / $ratio;
      }

      $img = theme_image(['path' => $file->uri, 'width' => $width, 'height' => $height, 'alt' => 'antibody_image','attributes' => []]);
    }
    else {
      $img = '';
    }
    return [
      '#markup' => $img,
      '#prefix' => '<div class="row">&nbsp;<div class="col-xs-5"> ',
      '#suffix' => '</div></div>',
    ];
  }

  /**
   * Saves the data of this image into the database.
   *
   * @throws \InvalidMergeQueryException
   */
  public function save() {
    AntibodyImageRepository::save($this);
  }
}
