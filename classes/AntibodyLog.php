<?php

class AntibodyLog {

  /**
   * @var int
   */
  private $id = -1;

  /**
   * @var int
   */
  private $antibodyId = Antibody::EMPTY_ANTIBODY_ID;

  /**
   * @var string
   */
  private $dateTime;

  /**
   * @var int
   */
  private $userId;

  /**
   * @var string
   */
  private $message = '-';

  /**
   * @var int (unsigned)
   */
  private $actionType;

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return int
   */
  public function getAntibodyId() {
    return $this->antibodyId;
  }

  /**
   * @param int $antibodyId
   */
  public function setAntibodyId($antibodyId) {
    $this->antibodyId = $antibodyId;
  }

  /**
   * @return string
   */
  public function getDateTime() {
    return $this->dateTime;
  }

  /**
   * @param string $dateTime
   */
  public function setDateTime($dateTime) {
    $this->dateTime = $dateTime;
  }

  /**
   * @return int
   */
  public function getUserId() {
    return $this->userId;
  }

  public function getUser() {
    return UsersRepository::findByUid($this->userId);
  }

  /**
   * @param int $userId
   */
  public function setUserId($userId) {
    $this->userId = $userId;
  }

  /**
   * @return string
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * @param string $message
   */
  public function setMessage($message) {
    $this->message = $message;
  }

  /**
   * @return int
   */
  public function getActionType() {
    return $this->actionType;
  }

  /**
   * @param int $actionType
   */
  public function setActionType($actionType) {
    $this->actionType = $actionType;
  }

  public function __construct() {
    global $user;

    $this->setDateTime(date("Y-m-d H:i:s"));
    $this->setUserId($user->uid);
  }

  public function save() {
    AntibodyLogsRepository::save($this);
  }

}

class AntibodyLogsRepository {
  static $tableName = 'antibody_log';
  static $databaseFields = array('id', 'antibody', 'datetime', 'user', 'message', 'action_type');

  /**
   * @param $result
   * @return \AntibodyLog
   */
  public static function databaseResultsToAntibodyLog($result) {
    $log = new AntibodyLog();

    if(empty($result))
      return $log;

    $log->setId($result->id);
    $log->setAntibodyId($result->antibody);
    $log->setDateTime($result->datetime);
    $log->setUserId($result->user);
    $log->setMessage($result->message);
    $log->setActionType($result->action_type);

    return $log;
  }

  /**
   * @param $results
   * @return \AntibodyLog[]
   */
  public static function databseResultsToAntibodyLogs($results) {
    $logs = array();

    foreach($results as $result)
      $logs[] = self::databaseResultsToAntibodyLog($result);

    return $logs;
  }

  /**
   * @param $antibodyLog
   * @return bool
   */
  public static function save($antibodyLog) {
    if($antibodyLog->getAntibodyId() == Antibody::EMPTY_ANTIBODY_ID)
      return false;

    if($antibodyLog->getId() == -1)
      $antibodyLog->setId(NULL);

    db_merge(self::$tableName)->key(array('id' => $antibodyLog->getId()))
      ->fields(array(
        'id' => $antibodyLog->getId(),
        'antibody' => $antibodyLog->getAntibodyId(),
        'datetime' => $antibodyLog->getDateTime(),
        'user' => $antibodyLog->getUserId(),
        'message' => $antibodyLog->getMessage(),
        'action_type' => $antibodyLog->getActionType()
      ))->execute();

    return true;
  }

  /**
   * @param $antibodyId
   * @param string $orderDirection
   * @return \AntibodyLog[]
   */
  public static function findByAntibodyId($antibodyId, $orderDirection = 'DESC') {
    $results = db_select(self::$tableName, 'l')
      ->condition('antibody', $antibodyId, '=')
      ->fields('l', self::$databaseFields)
      ->orderBy('datetime', $orderDirection)
      ->execute();

    return self::databseResultsToAntibodyLogs($results);
  }

}

class AntibodyLogActionType {
  const NONE = 0;
  const CREATE = 50;
  const MODIFY = 100;
  const ADD_APPLICATION = 200;
  const PUBLISH = 500;
}