CREATE TABLE `antibody_antibody2` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'An ID of the antibody',
  `type` varchar(10) DEFAULT 'primary' COMMENT 'Type of the antibody, can be primary or secondary. No value means also primary.',
  `working_group_id` int(11) unsigned NOT NULL COMMENT 'Owning working group',
  `locked` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'UID of user who is currently editing this antibody',
  `sharing_level` int(3) unsigned NOT NULL DEFAULT '0' COMMENT 'For a definition of sharing level see AntibodySharingLevel class',
  `last_modified_by` int(10) unsigned DEFAULT NULL COMMENT 'UID of a user who modified this entry. See table users',
  `created_by` int(10) unsigned DEFAULT NULL COMMENT 'UID of a user who created this entry',
  `created_date` date DEFAULT NULL COMMENT 'Creation date. Format: YYYY-MM-DD',
  `name` varchar(120) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Name of this antibody',
  `alt_name` varchar(120) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Alternative name of this antibody',
  `antigen_symbol` varchar(120) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Antigen symbol of this antibody',
  `registry_id` varchar(120) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Antibody Registry ID (online: antibodyregistry.org)',
  `demasking` varchar(10) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Values: unknown, yes, no',
  `clonality` varchar(12) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Values: unknown, polyclonal, monoclonal',
  `antigen` varchar(120) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Name of the antigen',
  `catalog_no` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Vendors catalog no',
  `lot_no` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Vendors lot no',
  `storage_instruction` text CHARACTER SET utf8 COMMENT 'Storage instructions (e.g. from antibody datasheet)',
  `localization` text CHARACTER SET utf8 COMMENT 'Current localization of the antibody (e.g. room, fridge)',
  `description` text CHARACTER SET utf8 COMMENT 'Description',
  `receipt_date` datetime DEFAULT NULL COMMENT 'Receipt date',
  `company_id` int(11) DEFAULT NULL COMMENT '',#deprecated
  `company` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT 'The name of the company',
  `crafted_by` varchar(120) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Values: unknown, self-made, company',
  `tag` varchar(120) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Tags',
  `raised_in_species` varchar(120) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Name of the species where this antibody is raised in',
  `isotype` varchar(120) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Isotype',
  `clone` varchar(120) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Clone',
  `antibodypedia_url` varchar(120) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Antibodypedia URL',
  `excitation_max` varchar(120) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Excitation max (secondary antibody only)',
  `emission_max` varchar(120) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Emission max (secondary antibody only)',
  `preparation_date` datetime DEFAULT NULL COMMENT 'Preparation date',
  `ready_for_submission` tinyint(4) DEFAULT NULL COMMENT 'Flag (true|false). Describes if this antibody has sufficiently descriptive data to be sent to other (external) repositories ',
  `export` tinyint(4) DEFAULT NULL COMMENT 'Flag(true|false). True if this antibody can be exported to external repositories.',
  PRIMARY KEY (`id`)
)  COMMENT='Research Data Platform - Antibody Catalogue - Antibody table';

CREATE TABLE `antibody_application` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `abbreviation` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT 'Application abbreviation (e.g. WB for Western Blot)',
  `name` text CHARACTER SET utf8 COMMENT 'Application name (e.g. Western Blot)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `abbreviation_UNIQUE` (`abbreviation`)
)  COMMENT='Research Data Platform - Antibody Catalogue - Application';

CREATE TABLE `antibody_antibody_application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `antibody_id` int(11) NOT NULL COMMENT 'Foreign value. ID of an antibody from antibody_antibody2 table',
  `application` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT 'Foreign value. Abbreviation of application',
  `working_group` int(11) NOT NULL COMMENT 'Foreign value. ID of a working group.',
  `dilution` varchar(45) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Dilution (e.g. 1:1000) for a given application',
  `quality` int(3) DEFAULT '0' COMMENT 'see AntibodyApplicationQuality class' COMMENT 'Usage quality for a given application (Values: 0 - not set, 1 - very bad,...,5 - very good)',
  `comment` text CHARACTER SET utf8 COMMENT 'Working group comment for a given application and antibody',
  `concentration_raw` varchar(45) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Concentration raw (e.g. 100 mg/ml)',
  `concentration_value` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Concentration value (e.g. 100)',
  `concentration_dimension` varchar(15) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Concentration dimension (e.g. mg/ml)',
  `wb_band` varchar(120) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Western Blot band',
  `created_by` int(10) unsigned DEFAULT NULL COMMENT 'UID of a creator (see users table)',
  `last_modified` int(10) unsigned DEFAULT NULL COMMENT 'UID of a user, who modified this entry (see users table)',
  PRIMARY KEY (`antibody_id`,`application`,`working_group`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) COMMENT='Research Data Platform - Antibody Catalogue - Description of the application usage of given antibody';

CREATE TABLE `antibody_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Log ID',
  `antibody` int(11) DEFAULT NULL COMMENT 'Antibody ID (see antibody_antibody2 table)',
  `datetime` datetime DEFAULT NULL COMMENT 'DateTime of log entry creation',
  `user` int(10) unsigned DEFAULT NULL COMMENT 'ID of the user triggering this log entry',
  `message` text CHARACTER SET utf8,
  `action_type` int(10) unsigned DEFAULT '0' COMMENT 'Code of the action',
  PRIMARY KEY (`id`)
) COMMENT='Research Data Platform - Antibody Catalogue - Log';

CREATE TABLE `antibody_species` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 COMMENT 'Species name',
  PRIMARY KEY (`id`)
) COMMENT='Research Data Platform - Antibody Catalogue - Species';

CREATE TABLE `antibody_antibody_reacts_with_species` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `antibody_id` int(11) NOT NULL,
  `species_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT='Research Data Platform - Antibody Catalogue - List of species reacting with a given antibody';

CREATE TABLE `antibody_statistics` (
  `antibody` int(11) NOT NULL,
  `views` int(11) DEFAULT '0',
  `anonymous_views` int(11) DEFAULT '0',
  PRIMARY KEY (`antibody`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
